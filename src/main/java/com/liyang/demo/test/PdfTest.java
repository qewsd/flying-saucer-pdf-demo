package com.liyang.demo.test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.ClassUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

public class PdfTest {
	public static String FILE_DIR = "D:/pdfvm2.pdf";
	public static String FONT_PATH = "font/SIMSUN.TTC";
	public static String VMPATH = "templates/pdf/test.html.vm";
	private final static TemplateEngine templateEngine = new TemplateEngine();

	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();
//		zip();
		createPdf(getHtml(VMPATH), new FileOutputStream(FILE_DIR));
		System.out.println("pdf finish, consume time:" + (System.currentTimeMillis() - start));
	}

	/**    
	 *   批量生成pdf，压缩文件
	 * @author liyang.yuan   
	 */
	public static void zip() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ZipOutputStream zip = new ZipOutputStream(outputStream);
		try {
			try {
				for (int i = 0; i < 100; i++) {
					// 添加到zip
					ByteArrayOutputStream out1 = new ByteArrayOutputStream();
					zip.putNextEntry(new ZipEntry("文件" + i + ".pdf"));
					createPdf(getHtml(VMPATH), out1);
					IOUtils.write(out1.toByteArray(), zip);
					IOUtils.closeQuietly(out1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			zip.flush();
			zip.closeEntry();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//
		String fileName = "D:/" + "pdf.zip";
		OutputStream out;
		try {
			out = new FileOutputStream(fileName);
			IOUtils.write(outputStream.toByteArray(), out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**    
	 * 创建pdf
	 * @author liyang.yuan
	 * @param html
	 * @return
	 * @throws com.lowagie.text.DocumentException
	 * @throws IOException   
	 */
	public static void createPdf(String html, OutputStream out) throws com.lowagie.text.DocumentException, IOException {
		/**
		 * 切记 css 要定义在head 里，否则解析失败
		 * css 要定义字体
		 * 字体中英文 https://www.cnblogs.com/chbyiming-bky/articles/9789869.html
		 * 例如宋体   style="font-family:SimSun" 	用		simsun.ttc   
		 */
		ITextRenderer renderer = new ITextRenderer();
		// 解决中文支持问题
		ITextFontResolver fontResolver = renderer.getFontResolver();
		// 字体名称要大写，否则可能找不到
		fontResolver.addFont(FONT_PATH, "Identity-H", false);
		renderer.setDocumentFromString(html);
		renderer.getSharedContext().setBaseURL("file:"+ClassUtils.getDefaultClassLoader().getResource("").getPath()+"static/");
		renderer.layout();
		renderer.createPDF(out);
	}

	/**    
	 * 根据模板生成文件
	 * @author liyang.yuan
	 * @param vmPath
	 * @return
	 * @throws IOException   
	 */
	public static String getHtml(String vmPath) throws IOException {
		Resource resource = new ClassPathResource(vmPath);
		File sourceFile = resource.getFile();
		Context context = new Context();
		Map<String, Object> params = new HashMap<>();
		context.setVariables(params);
		return templateEngine.process(getFileString(sourceFile), context);
	}

	/**    
	 * 读取文件
	 * @author liyang.yuan
	 * @param file   
	 */
	public static String getFileString(File file) {
		BufferedReader reader = null;
		StringBuffer sbf = new StringBuffer();
		try {
			reader = new BufferedReader(new FileReader(file));
			String tempStr;
			while ((tempStr = reader.readLine()) != null) {
				sbf.append(tempStr);
			}
			reader.close();
			return sbf.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return sbf.toString();
	}

}
